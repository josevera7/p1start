package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	
	private int currentSize;
	private Node<E> header;
	private Comparator<E> comp;
	
	public CircularSortedDoublyLinkedList(Comparator<E> cmp){
		this.header= new Node<E>(null,this.header,this.header);
		this.currentSize=0;
		this.comp=cmp;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new SCDLLIterator<E>();
	}

	@Override
	public boolean add(E obj) {
		Node<E> newNode= new Node<>(obj,null,null);
		if(this.isEmpty()) {
			newNode.setNext(header);
			newNode.setPrev(header);
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			this.currentSize++;
			return true;
		}
		int i = 0;
		for (Node<E> pointer = this.header.getNext(); (i<=this.currentSize); 
				pointer = pointer.getNext(), ++i) {
			if (pointer.equals(header)) {
				newNode.setNext(header);
				newNode.setPrev(header.getPrev());
				header.getPrev().setNext(newNode);
				header.setPrev(newNode);
				this.currentSize++;
				return true;
			}
			if(comp.compare(obj, pointer.getElement())<0) {
				newNode.setNext(pointer);
				newNode.setPrev(pointer.getPrev());
				pointer.getPrev().setNext(newNode);
				pointer.setPrev(newNode);
				this.currentSize++;
				return true;
			}
			
		}
		return false;
		
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		int index=this.firstIndex(obj);
		if(index<0) {
			return false;
		}
		remove(index);
		return true;
	}

	@Override
	public boolean remove(int index) {
		if ((index < 0) || (index >= this.currentSize)){
			throw new IndexOutOfBoundsException();
		}
		else {
			Node<E> target = this.getPosition(index);
			target.getPrev().setNext(target.getNext());
			target.getNext().setPrev(target.getPrev());
			target.setPrev(null);
			target.setNext(null);
			target.setElement(null);
			this.currentSize--;
			return true;
		}
	}

	@Override
	public int removeAll(E obj) {
		int count = 0;
		while (this.remove(obj)) {
			count++;
		}
		return count;
	}

	@Override
	public E first() {
		return (this.isEmpty()? null:this.header.getNext().getElement());
	}

	@Override
	public E last() {
		return (this.isEmpty()? null:this.header.getPrev().getElement());
	}

	@Override
	public E get(int index) {
		if ((index < 0) || index >= this.currentSize) {
			throw new IndexOutOfBoundsException();
		}
		
		Node<E> temp  = this.getPosition(index);
		return temp.getElement();
	}
	
	private Node<E> getPosition(int index){
		int currentPosition=0;
		Node<E> temp = this.header.getNext();
		while(currentPosition < index) {
			temp = temp.getNext();
			currentPosition++;
		}
		return temp;

	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		return this.firstIndex(e)>=0;
	}

	@Override
	public boolean isEmpty() {
		return this.size()==0;
	}

	@Override
	public int firstIndex(E e) {
		int i = 0;
		for (Node<E> temp = this.header.getNext(); !(i>=this.currentSize); 
				temp = temp.getNext(), ++i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// not found
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int i = currentSize-1;
		for (Node<E> temp = this.header.getPrev(); !(i<0); 
				temp = temp.getPrev(), --i) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// not found
		return -1;
	}
	
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> prev;
		
		public Node(E element, Node<E> next, Node<E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev= prev;
		}
		public Node() {
			super();
		}
		
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
		public Node<E> getPrev() {
			return prev;
		}
	}
	
	private class SCDLLIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		
		public SCDLLIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}
		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}

}
