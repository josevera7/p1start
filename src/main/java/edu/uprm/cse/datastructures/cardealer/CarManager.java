package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.CarList;

import java.util.Optional;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;


@Path("/cars")
public class CarManager {
	
	private CircularSortedDoublyLinkedList<Car> carList= CarList.getInstance();
	
	@GET
//	@Path("/cars")
	//@Produces(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] readAllCars() {
		Car[] cars= new Car[carList.size()];
		for(int i=0; i<carList.size();i++) {
			cars[i]=carList.get(i);
		}
	    return cars;
	}
	
	@GET
	  @Path("{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car readCar(@PathParam("id") long id){
		for(Car c:carList) {
			if(c.getCarId()==id) {
				return c;
			}
		}
	      throw new WebApplicationException(404);
	  }
	
	@POST
	  @Path("/add")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Response addCustomer(Car car){
	    carList.add(car);
	    return Response.status(201).build();
	  }
	
	@PUT
	  @Path("/{id}/update")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Response updateCustomer(@PathParam("id") long id,Car car){
		for(Car c:carList) {
			if(c.getCarId()==id) {
				carList.remove(c);
				carList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
	      return Response.status(Response.Status.NOT_FOUND).build();      
	  }
	
	@DELETE
	  @Path("/{id}/delete")
	  public Response deleteCustomer(@PathParam("id") long id){
	    for(Car c: carList) {
	    	if(c.getCarId()==id) {
	    		carList.remove(c);
	    		return Response.status(Response.Status.OK).build();  
	    	}
	    }
	      return Response.status(Response.Status.NOT_FOUND).build();      
	  }

}
