package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{
	
	String carString1;
	String carString2;
	
	 public CarComparator(){
		super();
	 }

	@Override
	public int compare(Car c1, Car c2) {
		// TODO Auto-generated method stub
		 carString1=c1.getCarBrand() + "-" + c1.getCarModel() + "-" + c1.getCarModelOption();
		 carString2=c2.getCarBrand() + "-" + c2.getCarModel() + "-" + c2.getCarModelOption();
		return carString1.compareTo(carString2);
	}

}
